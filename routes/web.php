<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/buildings', 'BuildingsController@index');
Route::get('/buildings/create', 'BuildingsController@create');
Route::post('/buildings', 'BuildingsController@store');
Route::get('/buildings/{id}', 'BuildingsController@show');
Route::get('/buildings/{id}/edit', 'BuildingsController@edit');
Route::patch('/buildings/{id}', 'BuildingsController@update');
Route::delete('/buildings/{id}', 'BuildingsController@destroy');


Route::group(['prefix' => 'inventory'], function () {
    Route::get('/', 'InventoryController@index');
    Route::get('/create', 'InventoryController@create');
    Route::post('/', 'InventoryController@store');
    Route::patch('/{id}', 'InventoryController@update');
    Route::get('/{id}/edit', 'InventoryController@edit');
    Route::get('/{id}', 'InventoryController@show');
    Route::delete('/{id}', 'InventoryController@destroy');
});