{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    
@stop

@section('content')


<div class="card">
	<div class="card-header">Buildings</div>
	<div class="card-body">
	    <a href="{{ url('/buildings/create') }}" class="btn btn-success btn-sm" title="Add New building">
	        <i class="fa fa-plus" aria-hidden="true"></i> Add New
	    </a>

	    <form method="GET" action="{{ url('/buildings') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
	        <div class="input-group">
	            <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
	            <span class="input-group-append">
	                <button class="btn btn-secondary" type="submit">
	                    <i class="fa fa-search"></i>
	                </button>
	            </span>
	        </div>
	    </form>

	    <br/>
	    <br/>
	    <div class="table-responsive">
	        <table class="table">
	            <thead>
	                <tr>
	                    <th>#</th><th>Name</th><th>Occupancy</th><th>Status</th><th>Actions</th>
	                </tr>
	            </thead>
	            <tbody>
	            @foreach($buildings as $item)
	                <tr>
	                    <td>{{ $loop->iteration }}</td>
	                    <td>{{ $item->name }}</td><td>{{ $item->occupancy }}</td><td>{{ $item->status }}</td>
	                    <td>
	                        <a href="{{ url('/buildings/' . $item->id) }}" title="View building"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
	                        <a href="{{ url('/buildings/' . $item->id . '/edit') }}" title="Edit building"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

	                        <form method="POST" action="{{ url('/buildings' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
	                            {{ method_field('DELETE') }}
	                            {{ csrf_field() }}
	                            <button type="submit" class="btn btn-danger btn-sm" title="Delete building" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
	                        </form>
	                    </td>
	                </tr>
	            @endforeach
	            </tbody>
	        </table>
	        <div class="pagination-wrapper"> {!! $buildings->appends(['search' => Request::get('search')])->render() !!} </div>
	    </div>

	</div>
</div>

@stop

@section('css')
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
@stop

@section('js')
    
@stop