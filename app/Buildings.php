<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buildings extends Model
{
    protected $fillable = ['name', 'occupancy', 'status'];
}
